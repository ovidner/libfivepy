from setuptools import setup, Extension
from setuptools.command.install import install
import subprocess
import os

class CustomInstall(install):
    def run(self):
        command = "git clone https://github.com/libfive/libfive.git libfiveSrc"
        process = subprocess.Popen(command, shell=True, cwd="./")
        process.wait()
        command = "hg clone https://bitbucket.org/eigen/eigen"
        process = subprocess.Popen(command, shell=True, cwd="./")
        process.wait()
        install.run(self)

libfive = Extension('libfive.libfiveKernel',
                   sources = ['libfiveSrc/libfive/src/libfive.cpp'],
                   include_dirs = ['libfiveSrc/libfive/include','eigen'],
                   extra_compile_args=['-fPIC'])

setup(
    name='libfive',
    version='0.1',
    packages=['libfive'],
    install_requires=[
    ],
    cmdclass={
        'install': CustomInstall,
        'develop': CustomInstall,
    },
    include_package_data=True,
    ext_modules=[libfive],
)
