from shapes import *

out = box(4) ^ sphere(20).move_z(10)
out -= 1

bound = 30
region = Region3((-bound,bound),(-bound,bound),(-bound,bound))
out.save_mesh(region, 10, "out.stl")
